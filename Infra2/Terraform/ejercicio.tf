terraform{
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "~> 5.0"
        }
    }
}

resource "aws_vpc" "vpc-prueba-infra2" {
    cidr_block = "192.168.0.0/22"
}

resource "aws_subnet" "subnet-public-1" {
    vpc_id                   =  aws_vpc.vpc-pruena-infra2.id 
    cidr_block               =  "192.168.0.0/24"
    availability_zone        =  "us-east-1a"
    map_public_ip_on_launch  =  true

    tags = {
        Name          =  "Subnet publica 1" 
    }
}

resource "aws_subnet" "subnet-public-2" {
    vpc_id                   =  aws_vpc.vpc-pruena-infra2.id 
    cidr_block               =  "192.168.1.0/24"
    availability_zone        =  "us-east-1b"
    map_public_ip_on_launch  =  true

    tags = {
        Name          =  "Subnet publica 2" 
    }
}

resource "aws_subnet" "subnet-private-1" {
    vpc_id                   =  aws_vpc.vpc-pruena-infra2.id 
    cidr_block               =  "192.168.2.0/24"
    availability_zone        =  "us-east-1a"
    map_public_ip_on_launch  =  false

    tags = {
        Name          =  "Subnet privada 1" 
    }
}

resource "aws_subnet" "subnet-private-2" {
    vpc_id                   =  aws_vpc.vpc-pruena-infra2.id 
    cidr_block               =  "192.168.2.0/24"
    availability_zone        =  "us-east-1b"
    map_public_ip_on_launch  =  false

    tags = {
        Name          =  "Subnet privada 2" 
    }
}

resource "aws_instance" "ec2-1" {
    ami                 = ami-0c7217cdde317cfec
    instance_type       = t2.micro
    availability_zone   = "us-east-1b"
    subnet_id           = aws_subnet.subnet-public-1.id
}

resource "aws_instance" "ec2-2" {
    ami                 = ami-0c7217cdde317cfec
    instance_type       = t2.micro
    availability_zone   = "us-east-1a"
    subnet_id           = aws_subnet.subnet-public-2.id
}
